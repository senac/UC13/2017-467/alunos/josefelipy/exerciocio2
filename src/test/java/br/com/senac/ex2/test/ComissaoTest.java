
package br.com.senac.Ex2.test;

import felipy.segatto.ex2.Comissao;
import org.junit.Test;
import static org.junit.Assert.*;

public class ComissaoTest {
    
    public ComissaoTest() {
        
        
    }
    
      @Test
    public void deveCalcularCustoFinalConsumidor(){
        Comissao comissao = new Comissao();
        double valorCusto = 10000;
        double resultado = comissao.calcular(valorCusto);
        
        assertEquals(17300, resultado , 0.01);
    }
    
}
